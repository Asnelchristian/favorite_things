import axios from 'axios';

const API_URL = 'https://favorite--things.herokuapp.com/api';


export function authenticate(userData) {
  return axios.post(`${API_URL}/login`, userData);
}

export function register(userData) {
  return axios.post(`${API_URL}/register`, userData);
}

export function fetchCategory(jwt, categoryName) {
  return axios.get(
    `${API_URL}/categories/${categoryName}`,
    { headers: { 'x-access-token': jwt.token } },
  );
}

export function fetchCategories(jwt, sortMethod, search) {
  return axios.get(
    `${API_URL}/categories/&/${sortMethod}/${search}`,
    { headers: { 'x-access-token': jwt.token } },
  );
}

export function postNewCategory(category, jwt) {
  return axios.post(
    `${API_URL}/addcategory`, category,
    { headers: { 'x-access-token': jwt.token } },
  );
}

export function editCategory(category, jwt) {
  return axios.put(
    `${API_URL}/editcategory`, category,
    { headers: { 'x-access-token': jwt.token } },
  );
}

export function deleteCategory(category, jwt) {
  return axios.delete(
    `${API_URL}/removecategory/${category.name}`,
    { headers: { 'x-access-token': jwt.token } },
  );
}

export function fetchFavorite(jwt, categoryName, rank) {
  return axios.get(
    `${API_URL}/categories/${categoryName}/${rank}`,
    { headers: { 'x-access-token': jwt.token } },
  );
}

export function fetchFavorites(jwt, categories, sortMethod, search) {
  return axios.get(
    `${API_URL}/favorites/${categories}/${sortMethod}/${search}`,
    { headers: { 'x-access-token': jwt.token } },
  );
}

export function postNewFavorite(favorite, jwt) {
  return axios.post(`${API_URL}/addfavorite`, favorite,
    { headers: { 'x-access-token': jwt.token } });
}

export function editFavorite(favorite, jwt) {
  return axios.put(`${API_URL}/editfavorite`, favorite,
    { headers: { 'x-access-token': jwt.token } });
}

export function deleteFavorite(favorite, jwt) {
  return axios.delete(
    `${API_URL}/removefavorite/${favorite.category}/${favorite.title}`,
    { headers: { 'x-access-token': jwt.token } },
  );
}

export function fetchAuditLogs(jwt) {
  return axios.get(
    `${API_URL}/logs`,
    { headers: { 'x-access-token': jwt.token } },
  );
}
