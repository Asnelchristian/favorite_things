export default {
  data() {
    return {
      metadata: [],
      metadataTypes: ['Number', 'Text', 'Date', 'Enum'],
    };
  },
  methods: {
    removeMetadata(index) {
      this.metadata.splice(index, 1);
    },
    addMetadata() {
      this.metadata.push({
        metadataKey: '',
        metadataType: 'Number',
        metadataValue: null,
      });
    },
    uniqueKeyCheck(index) {
      const key = this.metadata[index].metadataKey;
      if (key !== '') {
        const metadata = this.metadata.filter(
          (meta, idx) => meta.metadataKey === key && idx !== index,
        );
        if (metadata.length) {
          this.metadata[index].metadataKey = '';
          this.$notify({
            group: 'failure',
            type: 'error',
            text: 'Key has already been used',
          });
        }
      }
    },
  },
};
