export default {
  created() {
    this.metadata = [];
    this.loadData();
  },
  computed: {
    favorites() {
      return this.$store.state.favorites;
    },
    categories() {
      return this.$store.state.categories;
    },
    auditlogs() {
      return this.$store.state.auditlogs;
    },
    favoritesCount() {
      return this.$store.state.favorites.length;
    },
    categoriesCount() {
      return this.$store.state.categories.length;
    },
  },
  methods: {
    loadData() {
      this.$store.dispatch('loadFavorites');
      this.$store.dispatch('loadCategories');
      this.$store.dispatch('loadAuditLogs');
    },
  },
};
