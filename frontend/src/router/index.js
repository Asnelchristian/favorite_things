import Vue from 'vue';
import Router from 'vue-router';

import Home from '@/views/Home.vue';
import AuditLogs from '@/views/AuditLogs.vue';
import Category from '@/views/Category.vue';
import Categories from '@/views/Categories.vue';
import Favorite from '@/views/Favorite.vue';
import Favorites from '@/views/Favorites.vue';

import store from '@/store';
import { isValidJwt } from '@/utils';


Vue.use(Router);

function guard(to, from, next) {
  if (!isValidJwt(store.getters.jwt.token)) {
    next('/login');
  } else {
    next();
  }
}

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
    path: '/',
    redirect: '/favorites',
  }, {
    path: '/login',
    name: 'home',
    component: Home,
  }, {
    path: '/favorites',
    name: 'Favorites',
    component: Favorites,
    beforeEnter: guard,
  }, {
    path: '/categories',
    name: 'Categories',
    component: Categories,
    beforeEnter: guard,
  }, {
    path: '/categories/:categoryName',
    name: 'Category',
    component: Category,
    beforeEnter: guard,
  }, {
    path: '/categories/:categoryName/:rank',
    name: 'Favorite',
    component: Favorite,
    props: true,
    beforeEnter: guard,
  }, {
    path: '/auditlogs',
    name: 'AuditLogs',
    component: AuditLogs,
    beforeEnter: guard,
  }],
});
