import Vue from 'vue';
import Vuex from 'vuex';

import {
  register, fetchFavorites, fetchCategories, postNewCategory,
  fetchAuditLogs, postNewFavorite, authenticate, editCategory,
  deleteCategory, editFavorite, deleteFavorite,
} from '@/api';
import { EventBus } from '@/utils';

Vue.use(Vuex);


function initialState() {
  return {
    categories: [],
    favorites: [],
    username: '',
    auditlogs: [],
    jwt: '',
    baseSortMethods: [
      { name: 'Update time', value: 'modified_at' },
      { name: 'Creation time', value: 'created_at' }],
  };
}

const state = initialState();

const actions = {
  register(context, userData) {
    context.commit('setUserData', { userData });
    return register(userData)
      .then(() => context.dispatch('login', userData))
      .catch((error) => {
        EventBus.$emit('failedRegistering', error);
      });
  },
  login(context, userData) {
    context.commit('setUserData', { userData });
    return authenticate(userData)
      .then(response => context.commit('setJwtToken', { jwt: response.data }))
      .catch((error) => {
        EventBus.$emit('failedAuthentication', error);
        localStorage.removeItem('username');
        localStorage.removeItem('token');
      });
  },
  logout(context) {
    context.commit('reset');
    localStorage.removeItem('username');
    localStorage.removeItem('token');
  },
  loadCategories(context,
    filters = {
      sortMethod: 'modified_at&desc',
      search: '&',
    }) {
    return fetchCategories(context.getters.jwt,
      filters.sortMethod, filters.search)
      .then((response) => {
        context.commit('setCategories', response.data);
      });
  },
  loadFavorites(context,
    filters = {
      categories: '&',
      sortMethod: 'modified_at&desc',
      search: '&',
    }) {
    return fetchFavorites(context.getters.jwt, filters.categories,
      filters.sortMethod, filters.search)
      .then((response) => {
        context.commit('setFavorites', response.data);
      });
  },
  loadAuditLogs(context) {
    return fetchAuditLogs(context.getters.jwt)
      .then((response) => {
        context.commit('setAuditLogs', response.data);
      });
  },
  addNewCategory(context, category) {
    return postNewCategory(category, context.getters.jwt);
  },
  addNewFavorite(context, favorite) {
    return postNewFavorite(favorite, context.getters.jwt);
  },
  editCategory(context, category) {
    return editCategory(category, context.getters.jwt);
  },
  editFavorite(context, favorite) {
    return editFavorite(favorite, context.getters.jwt);
  },
  removeCategory(context, category) {
    return deleteCategory(category, context.getters.jwt);
  },
  removeFavorite(context, favorite) {
    return deleteFavorite(favorite, context.getters.jwt);
  },
};

const mutations = {
  reset(state) {
    // acquire initial state
    const s = initialState();
    Object.keys(s).forEach((key) => {
      state[key] = s[key];
    });
  },
  setCategories(state, payload) {
    state.categories = payload.categories;
  },
  setFavorites(state, payload) {
    state.favorites = payload.favorites;
  },
  setAuditLogs(state, payload) {
    state.auditlogs = payload.auditlogs;
  },
  setUserData(state, payload) {
    state.username = payload.userData.username;
    localStorage.setItem('username', payload.userData.username);
  },
  setJwtToken(state, payload) {
    localStorage.token = payload.jwt.token;
    state.jwt = payload.jwt;
  },
};

const getters = {
  // reusable data accessors
  jwt(state) {
    return { token: state.jwt.token || localStorage.getItem('token') };
  },
  user(state) {
    return state.username || localStorage.getItem('username');
  },
};

const store = new Vuex.Store({
  state,
  actions,
  mutations,
  getters,
});

export default store;
