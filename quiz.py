from cryptography.fernet import Fernet

key = 'TluxwB3fV_GWuLkR1_BzGs1Zk90TYAuhNMZP_0q4WyM='

# Oh no! The code is going over the edge! What are you going to do?
message = b'gAAAAABdIaDNIyrBtuESNqc49jmkALysWlN_D5N2nk_v3GBA-FmHJ-hb3lT_P8pL' \
          b'NAES0lwi-T_50ueUZ7_BGuC9M2RkLu09TDVcFpoiOX5Hog964FcMUJ6qGLxHDOrE' \
          b'OPQR49O5rzF_0YPOOMWun-xepoyJzlHsIBgZrkaDDUTdRs0GiuwpHxI='


def main():
    f = Fernet(key)
    print(f.decrypt(message))


if __name__ == "__main__":
    main()
