# Technical Questions

### Question 1

I have spent the last week working on the coding test (7days). 

With more time, the main feature I would have added is ``caching``. Right now every update or
page change makes an API call. Basically every time a page is rendered,
API calls are made to load data. Adding caching will greatly improved the user experience.

User data are not modifiable. Once a user name, password and email have been set
nothing can be done(by the user) to change them. With more time, I would have added mechanisms
for users to update their information, as well as recover their password using emails.

No logging mechanism has been implemented. In order to track down eventual issues, a logging system is needed.
With more I would have added it.

### Question 2
The most useful feature that was added recently to python (python 3.8) is described in [PEP 572](https://www.python.org/dev/peps/pep-0572/):
Assignment Expressions.

I used it to implement solution to [coding test](https://www.codewars.com/kata/54a91a4883a7de5d7800009c)
```python
def increment_string(strng):
    head = strng.rstrip('0123456789')
    if (tail := strng[len(head):]) == '':
        return ''.join([strng, '1'])
    return ''.join([head, str(int(tail) + 1).zfill(len(tail))])
```
(I have not used this version of python (python 3.8) on any project yet.)

### Question 3
In order to track performance issues in production, I would, with the collaboration of all stakeholders, run stress tests
on the product to identify the bottlenecks.
The first step will consist of isolating performance issues to an application tier. Then with comprehensive tests and logging
on the problematic tier I would track the root of the bottlenecks.

Doing this can terribly affect the user experience, so these tests should be monitored constantly (measuring end-user performance),
to ensure that load is supported by the live environment.
* Analytics should be reviewed to determine what the best to execute tests is
* Use a service virtualization or testing database

A good logging system can also be very informative while looking for the plausible bottlenecks.
I have never actually done this.