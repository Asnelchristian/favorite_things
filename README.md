# Favorite things

This project was developed using Vuejs, Flask, Python and MySQL.

These are the critical steps I took while working on this project
- Designing of the database. This is the first step but not the least during which I designed and implemented the database of the project from the requirements.
- API design and implementation. A TDD approach was used while implementing the Flask API. [Check this out for information on how to  test and lint the python](https://gitlab.com/Asnelchristian/favorite_things/blob/master/backend/README.md)
- Designing the user interface using scss and HTML
- SPA implementation using Vue.js:
    * Library for making API calls
    * State management using Vuex
    * Views(vue) implementation.
   
This project has been deployed on heroku.
The API and the SPA were deployed separately using git subtree: 
```bash
 git subtree push --prefix [dir] [remote-app-branch] master
```
- The API was deployed at [API link](https://favorite--things.herokuapp.com/)
- The SPA was deployed at [SPA](https://my-favorite-things.herokuapp.com)

```DATABASE_URL``` and ```APP_SETTINGS``` environment variables were set appropriately to deploy the API (to the clearDB database link and to ```production``` respectively.)
