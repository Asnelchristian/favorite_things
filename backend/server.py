from utils.app import create_app
from utils.settings import APP_SETTINGS


app = create_app(APP_SETTINGS)

if __name__ == '__main__':
    app.run()
