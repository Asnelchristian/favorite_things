
# Favorite things API

Flask REST API for favorite things.

## Test

To test this project you need to set a few environment variables: 

 - TEST_DATABASE_URL = link to the database you want to use for testing.
 - APP_SETTINGS = 'testing'

Simply run ``$ pytest`` in backend/ (directory).
## Lint
[coala](coala.io) was used to lint, for code quality.  The configuration defined for this project can be found in the [.coafile](https://gitlab.com/Asnelchristian/favorite_things/blob/master/backend/.coafile).
