"""
    Utilities for migrating and testing the database on the shell.
"""


from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from utils.app import create_app
from utils.models import db, User, Category, Favorite, AuditLog
from utils.settings import APP_SETTINGS


app = create_app(APP_SETTINGS)

migrate = Migrate(app, db)
manager = Manager(app)

# provide a migration utility command
manager.add_command('database', MigrateCommand)


# enable python shell with application context
@manager.shell
def shell_ctx():
    return dict(app=app,
                db=db,
                User=User,
                Category=Category,
                Favorite=Favorite,
                AuditLog=AuditLog)


if __name__ == '__main__':
    manager.run()
