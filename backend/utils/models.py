import sqlalchemy

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import event, inspect
from sqlalchemy.orm import class_mapper
from sqlalchemy.orm.attributes import get_history
from werkzeug.security import generate_password_hash

from .helpers import get_ref_name, get_owner_id

db = SQLAlchemy()

ACTION_CREATE = 'created'
ACTION_UPDATE = 'updated'
ACTION_DELETE = 'deleted'


class AuditLogMixin:
    """Change detection mixin.

    This class provides methods for listening to changes to a sqlalchemy
    database (insertion, update and deletion) and logging those changes
    to the ``AuditLogs`` table defined below.

    This is achieved by using the ``event`` object provided by sqlalchemy.
    """
    @classmethod
    def __declare_last__(cls):
        event.listen(cls, 'after_insert', cls.create_log)
        event.listen(cls, 'after_delete', cls.delete_log)
        event.listen(cls, 'after_update', cls.update_log)

    @staticmethod
    def create_log(mapper, connection, target):
        """Data creation/insertion event listener.
        """
        connection.execute(
            AuditLog.__table__.insert(),
            owner_id=get_owner_id(target),
            ref_orig=target.__tablename__,
            ref_id=target.id,
            ref_name=get_ref_name(target),
            timestamp=target.created_at,
            action=ACTION_CREATE)

    @staticmethod
    def delete_log(mapper, connection, target):
        """Data deletion event listener.
        """
        connection.execute(
            AuditLog.__table__.insert(),
            owner_id=get_owner_id(target),
            ref_orig=target.__tablename__,
            ref_id=target.id,
            ref_name=get_ref_name(target),
            action=ACTION_DELETE)

    @staticmethod
    def update_log(mapper, connection, target):
        """Data modification event listener.
        """
        state_before = {}
        state_after = {}
        inspected = inspect(target)
        attrs = class_mapper(target.__class__).column_attrs
        for attr in attrs:
            if attr.key == 'meta_type':
                continue
            hist = getattr(inspected.attrs, attr.key).history
            if hist.has_changes():
                state_before[attr.key] = get_history(target, attr.key)[2].pop()
                state_after[attr.key] = getattr(target, attr.key)

        if state_after != state_before:
            connection.execute(
                AuditLog.__table__.insert(),
                owner_id=get_owner_id(target),
                ref_orig=target.__tablename__,
                ref_name=get_ref_name(target),
                ref_id=target.id,
                affected=', '.join(state_before.keys()),
                change_from=', '.join((str(val)
                                       for val in state_before.values())),
                change_to=', '.join((str(val) for val in state_after.values())),
                action=ACTION_UPDATE)


class AuditLog(db.Model):

    __tablename__ = 'auditlog'

    id = db.Column(db.Integer, primary_key=True)
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    ref_orig = db.Column(db.String(150), doc='Type of the logged data.')
    ref_id = db.Column(db.Integer, doc='Id of the logged data')
    ref_name = db.Column(
        db.String(50), doc='Title or name if any of the target.')
    timestamp = db.Column(
        db.DateTime, default=sqlalchemy.func.now(), doc='Transaction time.')
    affected = db.Column(db.Text, doc='Fields affected by transaction.')
    change_from = db.Column(db.Text, doc='Previous values of affected fields.')
    change_to = db.Column(db.Text, doc='Updated values of affected fields.')
    action = db.Column(db.String(10), doc='Type of transaction or action.')

    def to_dict(self):
        return dict(id=self.id,
                    ref_orig=self.ref_orig,
                    ref_id=self.ref_id,
                    ref_name=self.ref_name,
                    timestamp=self.timestamp,
                    affected=self.affected,
                    change_from=self.change_from,
                    change_to=self.change_to,
                    action=self.action)


class User(AuditLogMixin, db.Model):

    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    created_at = db.Column(db.DateTime, default=sqlalchemy.func.now())
    modified_at = db.Column(db.DateTime, default=sqlalchemy.func.now())
    categories = db.relationship('Category', backref='owner', lazy=False)
    favorites = db.relationship('Favorite', backref='owner', lazy=False)
    auditlogs = db.relationship('AuditLog', backref='owner', lazy=False)

    def __init__(self, username, email, password):
        self.username = username
        self.email = email
        self.password = generate_password_hash(password, method='sha256')

    def to_dict(self):
        return dict(username=self.username, email=self.email,
                    created_at=self.created_at, modified_at=self.modified_at)

    def __repr__(self):
        return f'User({self.to_dict()})'


def add_base_categories(user):
    food = Category(name='Food', owner=user)
    person = Category(name='Person', owner=user)
    place = Category(name='Place', owner=user)
    db.session.add(food)
    db.session.add(person)
    db.session.add(place)
    db.session.commit()


class Category(AuditLogMixin, db.Model):

    __tablename__ = 'category'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    description = db.Column(db.Text, nullable=True)
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    created_at = db.Column(db.DateTime, default=sqlalchemy.func.now())
    modified_at = db.Column(db.DateTime, default=sqlalchemy.func.now())
    favorites = db.relationship('Favorite', backref='category',
                                cascade='delete,all,delete-orphan', lazy=False)
    meta = db.Column(db.Text, nullable=True,
                     doc='JSON string of metadata')

    def to_dict(self):
        favorites = Favorite.query.filter_by(owner_id=self.owner_id)
        count = favorites.filter_by(category=self).count()
        try:
            percentage = count * 100 / favorites.count()
        except ZeroDivisionError:
            percentage = 0
        return {
            'name': self.name,
            'description': self.description,
            'meta': self.meta if self.meta else None,
            'count': count,
            'percentage': percentage,
            'modified_at': self.modified_at,
            'created_at': self.created_at
        }

    def __repr__(self):
        return f'Category({self.to_dict()})'


class Favorite(AuditLogMixin, db.Model):

    __tablename__ = 'favorite'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), unique=True, nullable=False)
    description = db.Column(db.Text, nullable=True)
    rank = db.Column(db.Integer, default=1, nullable=False)
    created_at = db.Column(db.DateTime, default=sqlalchemy.func.now())
    modified_at = db.Column(db.DateTime, default=sqlalchemy.func.now())
    category_id = db.Column(db.Integer, db.ForeignKey(
        'category.id'), nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    meta = db.Column(db.Text, nullable=True,
                     doc='JSON string of metadata')

    def to_dict(self):
        return dict(title=self.title,
                    description=self.description,
                    category=self.category.name,
                    created_at=self.created_at,
                    modified_at=self.modified_at,
                    rank=self.rank,
                    meta=self.meta)

    def __repr__(self):
        return f'Favorite({self.to_dict()})'
