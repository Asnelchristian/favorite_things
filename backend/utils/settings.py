import os

from os.path import join, dirname
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

APP_SETTINGS = os.getenv('APP_SETTINGS')
SECRET_KEY = os.getenv('SECRET_KEY')
DEV_SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL')
TEST_SQLALCHEMY_DATABASE_URI = os.getenv('TEST_DATABASE_URL')
