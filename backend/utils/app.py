"""
    Define function for creating a flask app instance.
"""

from flask import Flask
from flask_cors import CORS

from .api import api
from .config import app_config
from .models import db


def create_app(config_name, app_name='Favorites'):
    app = Flask(app_name)
    app.config.from_object(app_config[config_name])

    CORS(app, resources={r'/api/*': {'origins': '*'}})

    app.register_blueprint(api, url_prefix='/api')
    db.init_app(app)

    return app
