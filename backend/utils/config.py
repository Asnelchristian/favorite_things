"""
    Define all the settings for the flask app.
"""

from .settings import (
    SECRET_KEY, DEV_SQLALCHEMY_DATABASE_URI, TEST_SQLALCHEMY_DATABASE_URI)


class BaseConfig:
    """Parent configuration class.
    """
    DEBUG = False

    SECRET_KEY = SECRET_KEY
    SQLALCHEMY_DATABASE_URI = DEV_SQLALCHEMY_DATABASE_URI
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(BaseConfig):
    """Configurations for Development.
    """
    DEBUG = True


class TestingConfig(BaseConfig):
    """Configurations for Testing, with a separate test database.
    """
    TESTING = True
    SQLALCHEMY_DATABASE_URI = TEST_SQLALCHEMY_DATABASE_URI
    DEBUG = True


class StagingConfig(BaseConfig):
    """Configurations for Staging.
    """
    DEBUG = True


class ProductionConfig(BaseConfig):
    """Configurations for Production.
    """
    DEBUG = False
    TESTING = False


app_config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'staging': StagingConfig,
    'production': ProductionConfig,
}
