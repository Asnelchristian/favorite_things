"""
    Defines a few helper functions used in the project.
"""


def get_ref_name(db_row):
    return (f'{vars(db_row).get("username", "")}'
            f'{vars(db_row).get("name", "")}'
            f'{vars(db_row).get("title", "")}')


def get_owner_id(db_row):
    try:
        return db_row.owner_id
    except AttributeError:
        return db_row.id


def update_rank(list_of_favorite_things, by=1):
    for favorite_thing in list_of_favorite_things:
        favorite_thing.rank += by
