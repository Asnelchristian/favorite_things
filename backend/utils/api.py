import jwt
import sqlalchemy

from functools import wraps
from datetime import datetime, timedelta
from flask import Blueprint, request, jsonify, current_app, make_response
from werkzeug.security import check_password_hash
from sqlalchemy import or_, desc

from .models import User, Category, Favorite, AuditLog, db, add_base_categories
from .helpers import update_rank


api = Blueprint('api', __name__)


def token_required(f):
    """Token required decorator.

    Wraps ``f`` with a token checking mechanism.
    Fires its execution only if a valid token is provided in the
    header of the request sent to ``f``.
    """
    @wraps(f)
    def decorated(*args, **kwargs):
        token = request.headers.get('x-access-token')

        invalid_msg = {
            'WWW-Authenticate': 'Invalid token, Login required!'
        }
        expired_msg = {
            'WWW-Authenticate': 'Expired token. Login required!',
        }

        if not token:
            return make_response('Could not verify', 401, invalid_msg)

        try:
            data = jwt.decode(token, current_app.config['SECRET_KEY'])
            current_user = User.query.filter_by(
                username=data['username']).first()
            if not current_user:
                raise RuntimeError('User not found')
        except jwt.ExpiredSignatureError:
            return make_response('Could not verify', 401, expired_msg)
        except jwt.InvalidTokenError:
            return make_response('Could not verify', 401, invalid_msg)
        except Exception as e:
            return jsonify({'message': 'Operation failed',
                            'exception': str(e)}), 401

        return f(current_user, *args, **kwargs)

    return decorated


@api.route('/register', methods=['POST'])
def create_user():
    """Endpoint for creating a new user.

    Takes the ``username``, ``email`` and ``password`` and add the user to
    the database.

    Also add the base categories for every user notably: ``Food``,
    ``Person`` and ``Place``.
    """
    data = request.get_json()
    new_user = User(username=data['username'],
                    email=data['email'], password=data['password'])

    db.session.add(new_user)
    db.session.commit()

    # Add base categories.
    add_base_categories(new_user)

    return jsonify(new_user.to_dict()), 201


@api.route('/login', methods=['POST'])
def login():
    """Endpoint for logging.

    Takes a ``username`` and ``password``, checks if the user exists and
    returns a jwt authentication token if the provided information are
    correct.

    Returns:
        401 In case of failure
        200 In case of success
    """
    auth = request.get_json()

    if not auth or not auth.get('username') or not auth.get('password'):
        return jsonify({'message': 'Invalid credentials',
                        'authenticated': False}), 401

    user = User.query.filter_by(username=auth['username']).first()

    if not user:
        return jsonify({'message': 'Invalid credentials',
                        'authenticated': False}), 401

    if check_password_hash(user.password, auth['password']):
        token = jwt.encode({
            'username': user.username,
            'iat': datetime.utcnow(),
            'exp': datetime.utcnow() + timedelta(minutes=30)},
            current_app.config['SECRET_KEY'])

        return jsonify({'token': token.decode('UTF-8')})
    return jsonify({'message': 'Invalid credentials',
                    'authenticated': False}), 401


@api.route('/categories')
@api.route('/categories/&/<sort_method>/<search>')
@token_required
def get_categories(current_user, sort_method='modified_at&desc', search='&'):
    """Endpoint for getting all the categories of a user.

    Categories are ordered by the method provided in ``sort_method``
    (the arrangement can is set as in the default value above: ``desc``).

    Categories are filtered by the ``search`` string. (Occurrences for
    ``search`` are looked for in the categories description and name.)

    :param
        current_user:   The current authenticated user
        sort_method:    Define how to sort categories
        search:         Define how to filter categories

    :returns
        A list of sorted and filtered categories.
    """

    _categories = Category.query.filter_by(owner=current_user)
    if search != '&':
        _categories = _categories.filter(
            or_(Category.name.contains(search),
                Category.description.contains(search)))
    order_by, order = sort_method.split('&')
    if order_by != 'fav_count':
        if order != 'desc':
            _categories = _categories.order_by(order_by)
        else:
            _categories = _categories.order_by(desc(order_by))
        categories = [category.to_dict() for category in _categories.all()]
    else:
        categories = [category.to_dict() for category in _categories.all()]
        categories.sort(
            key=lambda category: category['count'],
            reverse=(order == 'desc'))

    return jsonify({'categories': categories}), 200


@api.route('/addcategory', methods=['POST'])
@token_required
def add_category(current_user):
    """Endpoint for adding a new category.

    Takes a category name, description and possible metadata and
    add it to the database. Category names are unique for each user.

    :param
        current_user:   The current authenticated user

    :returns
        201 response if operation is a success
        202 response if operation fails.
    """
    data = request.get_json()
    category = Category.query.filter_by(
        name=data['name'], owner=current_user).all()
    if category:
        return jsonify(
            {'message': 'You already have a category of that name.'}), 202

    category = Category(
        name=data['name'], description=data['description'],
        owner=current_user, meta=data.get('meta'))
    db.session.add(category)
    db.session.commit()

    return jsonify({'message': 'New category created!'}), 201


@api.route('/removecategory/<category_name>', methods=['DELETE'])
@token_required
def remove_category(current_user, category_name):
    """Endpoint for removing a category.

    Takes a category name and deletes it from the database.

    :param
        current_user:   The current authenticated user
        category_name:  Name of the category to be removed.

    :returns
        200 response if operation is a success
        404 response if category doesn't exist.
    """
    category = Category.query.filter_by(
        name=category_name, owner=current_user).first()
    if not category:
        return jsonify({'message': 'No such category'}), 404

    db.session.delete(category)
    db.session.commit()
    return jsonify({'message': 'Category successfully removed!'}), 200


@api.route('/editcategory', methods=['PUT'])
@token_required
def edit_category(current_user):
    """Endpoint for editing a new category.

    Takes a category name, its new name, description and possibly
    metadata and update it accordingly.

    :param
        current_user:   The current authenticated user

    :returns
        200 response if operation is a success
        404 response if category doesn't not exist.
        202 response if new category already exist in db.
    """
    data = request.get_json()
    category = Category.query.filter_by(
        name=data['name'], owner=current_user).first()
    if not category:
        return jsonify({'message': 'No such category'}), 404

    name = data.get('new_name')
    if name and name != category.name:
        if Category.query.filter_by(name=name, owner=current_user).first():
            return jsonify(
                {'message':
                    'Can not update name to an existing category'}), 202
    else:
        name = category.name

    category.name = name
    category.description = data.get('new_description', category.description)
    category.meta = data.get('meta', category.meta)
    category.modified_at = sqlalchemy.func.now()
    db.session.commit()

    return jsonify({'message': 'Category updated!',
                    'category': category.name}), 200


@api.route('/favorites', methods=['GET'])
@api.route('/favorites/<filters>/<sort_method>/<search>', methods=['GET'])
@token_required
def get_favorites(current_user, filters=None,
                  sort_method='modified_at&desc', search='&'):
    """Endpoint for getting all the favorite things of a user.

    Favorites are ordered by the method provided in ``sort_method``
    (the arrangement can is set as in the default value above: ``desc``).

    Favorites are filtered by the ``search`` string. (Occurrences for
    ``search`` are looked for in the favorites' description and name.)

    :param
        current_user:   The current authenticated user
        sort_method:    Define how to sort favorites
        search:         Define how to filter favorites

    :returns
        A list of sorted and filtered Favorites.
    """

    _favorites = Favorite.query.filter_by(owner=current_user)
    if filters and filters != '&':
        filter_clauses = [
            (Favorite.category ==
             Category.query.filter_by(name=_filter, owner=current_user).first())
            for _filter in filters.split('&')]
        _favorites = _favorites.filter(or_(*filter_clauses))

    if search != '&':
        _favorites = _favorites.filter(
            or_(Favorite.title.contains(search),
                Favorite.description.contains(search)))

    order_by, order = sort_method.split('&')
    if order == 'desc':
        favorites = [fav.to_dict()
                     for fav in _favorites.order_by(desc(order_by)).all()]
    else:
        favorites = [fav.to_dict()
                     for fav in _favorites.order_by(order_by).all()]

    return jsonify({'favorites': favorites}), 200


@api.route('/addfavorite', methods=['POST'])
@token_required
def add_favorite_thing(current_user):
    """Endpoint for adding a new favorite.

    Takes a favorite title, description, category (name), rank and
    possibly metadata and add it to the database. Favorites titles
    are unique for each user.

    :param
        current_user:   The current authenticated user

    :returns
        201 response if operation is a success
        404 response if favorite's category doesn't not exist.
    """
    data = request.get_json()

    category = Category.query.filter_by(
        name=data['category']['name'], owner=current_user).first()
    if not category:
        return jsonify({'message': 'Category not found'}), 404

    favorites = Favorite.query.filter_by(category=category)
    rank = int(data['rank'])

    if rank > favorites.count():
        rank = favorites.count() + 1

    _lower_ranked_favorites = favorites.filter(Favorite.rank >= rank).all()

    for _favorite in _lower_ranked_favorites:
        _favorite.rank += 1

    favorite = Favorite(
        title=data['title'],
        description=data['description'],
        rank=rank,
        category=category,
        owner=current_user,
        meta=data.get('meta'))
    db.session.add(favorite)
    db.session.commit()
    return jsonify({'message': 'New favorite_thing created!'}), 201


@api.route('/removefavorite/<category_name>/<title>', methods=['DELETE'])
@token_required
def remove_favorite(current_user, category_name, title):
    """Endpoint for removing a favorite.

    Takes a favorite's title and category and deletes it from the database.

    :param
        current_user:   The current authenticated user
        category_name:  Category of the favorite to be removed.
        title:          Title of the favorite to be removed.

    :returns
        200 response if operation is a success
        404 response if favorite doesn't exist.
    """
    category = Category.query.filter_by(
        name=category_name, owner=current_user).first()

    favorites = Favorite.query.filter_by(category=category)
    favorite = favorites.filter_by(title=title).first()
    try:
        _lower_ranked_favorites = favorites.filter(
            Favorite.rank > favorite.rank).all()

        for _favorite in _lower_ranked_favorites:
            _favorite.rank -= 1
        db.session.delete(favorite)
        db.session.commit()
    except AttributeError:
        return jsonify({'message': 'No such thing'}), 404
    return jsonify({'message': 'Favorite successfully removed!'}), 200


@api.route('/editfavorite', methods=['PUT'])
@token_required
def edit_favorite(current_user):
    """Endpoint for editing a favorite.

    Takes a favorite's title and category, and update its attributes:
    title, description and metadata.

    :param
        current_user:   The current authenticated user

    :returns
        200 response if operation is a success
        404 response if favorite doesn't not exist.
        202 response if new favorite's title is already in use in the db.
    """
    data = request.get_json()

    category = Category.query.filter_by(name=data['category'],
                                        owner=current_user).first()
    favorites = Favorite.query.filter_by(category=category, owner=current_user)

    favorite = favorites.filter_by(title=data['title']).first()
    if not favorite:
        return jsonify({'message': 'No such thing'}), 404

    title = data.get('new_title', favorite.title)
    if (title and title != favorite.title) and \
            favorites.filter_by(title=title).first():
        return jsonify({'message': 'A favorite of that name exist'}), 202

    new_rank = int(data.get('new_rank', favorite.rank))

    # Ensure that the rank does not exceed the the number of favorites.
    if new_rank > favorites.count():
        new_rank = favorites.count()

    # Decrease or Increase the rank of all the favorites with a rank between
    # ``old rank`` and ``new_rank``.
    # Decrease if old rank < new rank
    # Increase if old rank > new rank
    if new_rank > favorite.rank:
        _lower_ranked_favorites = favorites.filter(
            Favorite.rank > favorite.rank,
            Favorite.rank <= new_rank).all()
        update_rank(_lower_ranked_favorites, by=-1)
    elif new_rank < favorite.rank:
        _upper_ranked_favorites = favorites.filter(
            Favorite.rank < favorite.rank,
            Favorite.rank >= new_rank).all()
        update_rank(_upper_ranked_favorites, by=1)

    favorite.rank = new_rank
    favorite.title = title
    favorite.description = data.get('new_description', favorite.description)

    favorite.meta = data.get('meta') or favorite.meta
    favorite.modified_at = sqlalchemy.func.now()

    db.session.commit()
    return jsonify({'message': 'favorite successfully updated!'}), 200


@api.route('/categories/<category_name>', methods=['GET'])
@api.route('/categories/<category_name>/<rank>', methods=['GET'])
@token_required
def get_category_favorites_and_associated_logs(current_user,
                                               category_name, rank=None):
    """Endpoint for getting all information on a category or a favorite.
    :param
        current_user:   The current authenticated user
        category_name:  Name of the category.
        rank:           Rank of the favorite.

    :returns
        200 response if operation is a success
        404 response if favorite or category do not exist.
    """
    category = Category.query.filter_by(
        name=category_name, owner=current_user).first()
    if not category:
        return jsonify({'message': 'No such category'}), 404
    if rank:
        favorite = Favorite.query.filter_by(
            category=category, rank=rank).first()
        if not favorite:
            return jsonify({'message': 'No such thing!'}), 404
        logs = [
            log.to_dict() for log in
            AuditLog.query.filter_by(ref_id=favorite.id,
                                     ref_orig=Favorite.__tablename__).all()]
        return jsonify({'favorite': favorite.to_dict(),
                        'auditlogs': logs}), 200

    favorites = Favorite.query.filter_by(
        owner=current_user, category=category).all()
    auditlogs = AuditLog.query.filter_by(owner=current_user)
    logs = []
    for fav in favorites:
        logs.extend(
            [log.to_dict() for log in
             auditlogs.filter_by(ref_id=fav.id,
                                 ref_orig=Favorite.__tablename__).all()])
    logs.extend(
        [log.to_dict() for log in
         auditlogs.filter_by(ref_id=category.id,
                             ref_orig=Category.__tablename__).all()])
    favorites = [fav.to_dict() for fav in favorites]
    return jsonify({'category': category.to_dict(),
                    'favorites': favorites,
                    'auditlogs': logs}), 200


@api.route('/logs', methods=['GET'])
@token_required
def get_logs(current_user):
    """Endpoint for getting logs on all modification on a user's data.

    :param
        current_user:   The current authenticated user

    :returns
        200 response if operation is a success.
    """
    auditlogs = AuditLog.query.filter_by(
        owner=current_user).order_by(desc(AuditLog.timestamp)).all()
    logs = []
    for log in auditlogs:
        logs.append(log.to_dict())
    return jsonify({'auditlogs': logs}), 200
