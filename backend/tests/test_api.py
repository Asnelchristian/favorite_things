import json
import pytest

from sqlalchemy.exc import IntegrityError

from utils.app import create_app
from utils.models import db


@pytest.fixture
def client(request):
    app = create_app(config_name='testing')
    client = app.test_client
    with app.app_context():
        db.create_all()

    def fin():
        with app.app_context():
            db.drop_all()
    request.addfinalizer(fin)

    yield client


@pytest.fixture
def user():
    yield {'username': 'john',
           'email': 'johndoe@gmail.com',
           'password': 'password'}


def login(client, user):
    res = client().post('api/login', json=user)
    res_data = json.loads(res.data.decode('utf-8').replace("'", '\"'))
    return res_data['token']


def test_user_creation(client, user):
    res = client().post('/api/register', json=user)
    assert res.status_code == 201

    with pytest.raises(IntegrityError):
        client().post('/api/register', json=user)


def test_user_logging(client, user):
    client().post('/api/register', json=user)

    # Valid user logging.
    res = client().post('/api/login', json=user)
    res_data = json.loads(res.data.decode('utf-8').replace("'", '\"'))
    assert res.status_code == 200
    assert res_data['token']
    assert len(res_data['token'].split('.')) == 3

    # Request without a username
    res = client().post('/api/login',
                        json=dict(password='password'))
    assert res.status_code == 401

    # Request without a password
    res = client().post('/api/login', json=dict(username='john'))
    assert res.status_code == 401

    # Wrong password
    res = client().post('/api/login',
                        json=dict(username='john', password='pass'))
    assert res.status_code == 401

    res_data = json.loads(res.data.decode('utf-8').replace("'", '\"'))
    assert {'message': 'Invalid credentials',
            'authenticated': False} == res_data


def test_get_categories(client, user):
    client().post('/api/register', json=user)
    token = login(client, user)

    # No filtering
    res = client().get('/api/categories', headers={'x-access-token': token})
    res_data = json.loads(res.data.decode('utf-8').replace("'", '\"'))

    assert res_data['categories']
    assert len(res_data['categories']) == 3
    assert {category['name'] for category in res_data['categories']} == {
        'Food', 'Place', 'Person'}
    assert {category['count'] for category in res_data['categories']} == {0}
    assert {category['percentage']
            for category in res_data['categories']} == {0}

    # Filtering
    res_ = client().get('/api/categories/&/modified_at&desc/&',
                        headers={'x-access-token': token})
    assert res_.status_code == 200
    assert res.data == res_.data

    # Filtering (search ``person``)
    res_ = client().get('/api/categories/&/modified_at&desc/person',
                        headers={'x-access-token': token})
    res_data = json.loads(res_.data.decode('utf-8').replace("'", '\"'))
    assert res_.status_code == 200
    assert {category['name'] for category in res_data['categories']} == {
        'Person'}

    # Filtering (search ``p``)

    res_ = client().get('/api/categories/&/modified_at&desc/p',
                        headers={'x-access-token': token})
    res_data = json.loads(res_.data.decode('utf-8').replace("'", '\"'))
    assert res_.status_code == 200
    assert {category['name'] for category in res_data['categories']} == {
        'Person', 'Place'}


def test_add_category(client, user):
    client().post('/api/register', json=user)
    token = login(client, user)

    # Add existing category: Person
    res = client().post('/api/addcategory', json=dict(name='person'),
                        headers={'x-access-token': token})
    assert res.status_code == 202  # Failure

    # Add a new category
    entertainment_category = {
        'name': 'Entertainment',
        'description': 'Will do these most of the time!',
    }
    res = client().post('/api/addcategory', json=entertainment_category,
                        headers={'x-access-token': token})
    assert res.status_code == 201  # Success

    # Get categories
    res = client().get('/api/categories', headers={'x-access-token': token})
    res_data = json.loads(res.data.decode('utf-8').replace("'", '\"'))
    assert {category['name'] for category in res_data['categories']} == {
                'Person', 'Place', 'Food', 'Entertainment'}


def test_remove_category(client, user):
    client().post('/api/register', json=user)
    token = login(client, user)

    # Remove non existing category: Nothing
    res = client().delete('/api/removecategory/nothing',
                          headers={'x-access-token': token})
    assert res.status_code == 404  # Failure

    # Remove an existing category: Person
    res = client().delete('/api/removecategory/person',
                          headers={'x-access-token': token})
    assert res.status_code == 200  # Success

    # Get categories
    res = client().get('/api/categories', headers={'x-access-token': token})
    res_data = json.loads(res.data.decode('utf-8').replace("'", '\"'))
    assert {category['name'] for category in res_data['categories']} == {
                'Place', 'Food'}


def test_edit_category(client, user):
    client().post('/api/register', json=user)
    token = login(client, user)

    # Edit a non existing category: Nothing
    res = client().put('/api/editcategory',
                       json={'name': 'nothing', 'new_name': "Don't care"},
                       headers={'x-access-token': token})
    assert res.status_code == 404  # Failure

    # Edit existing category: change name to existing name
    res = client().put('/api/editcategory',
                       json={'name': 'person', 'new_name': 'food'},
                       headers={'x-access-token': token})
    assert res.status_code == 202  # Failure

    # Edit existing category
    res = client().put('/api/editcategory',
                       json={'name': 'person', 'new_name': 'My people',
                             'new_description': 'They mean a lot to me!'},
                       headers={'x-access-token': token})
    assert res.status_code == 200  # Success

    # Get categories
    res = client().get('/api/categories', headers={'x-access-token': token})
    res_data = json.loads(res.data.decode('utf-8').replace("'", '\"'))
    assert {category['name'] for category in res_data['categories']} == {
                'My people', 'Place', 'Food'}

    my_people = [category for category in res_data['categories']
                 if category['name'] == 'My people']
    assert my_people[0]['description'] == 'They mean a lot to me!'


def test_get_favorites(client, user):
    client().post('/api/register', json=user)
    token = login(client, user)

    # No filtering
    res = client().get('/api/favorites', headers={'x-access-token': token})
    res_data = json.loads(res.data.decode('utf-8').replace("'", '\"'))

    assert res.status_code == 200
    assert res_data['favorites'] == []

    # Filtering
    res_ = client().get('/api/favorites/&/modified_at&desc/&',
                        headers={'x-access-token': token})
    assert res.data == res_.data


def test_add_favorite(client, user):
    client().post('/api/register', json=user)
    token = login(client, user)

    fav_new_york = {
        'title': 'New York',
        'description': 'Beautiful town!',
        'category': {'name': 'place'},
        'rank': 1
    }
    fav_guido = {
        'title': 'Guido Van Rossum',
        'description': 'Created a Wonderful language',
        'category': {'name': 'person'},
        'rank': 1
    }
    fav_pizza = {
        'title': 'Pizza',
        'description': 'Lunch time!',
        'category': {'name': 'Food'},
        'rank': 1
    }
    fav_coffee = {
        'title': 'Coffee',
        'description': 'To code!',
        'category': {'name': 'Food'},
        'rank': 1
    }
    fav_dummy = {
        'title': 'dummy',
        'rank': 2,
        'category': {'name': 'dummy'}
    }
    # Add favorite: Non existing category
    res = client().post('/api/addfavorite', json=fav_dummy,
                        headers={'x-access-token': token})
    assert res.status_code == 404  # Failure

    # Add Favorites
    res = client().post('/api/addfavorite', json=fav_new_york,
                        headers={'x-access-token': token})
    assert res.status_code == 201  # Success
    res = client().post('/api/addfavorite', json=fav_pizza,
                        headers={'x-access-token': token})
    assert res.status_code == 201  # Success

    # coffee will be ranked 1 and pizza 2 for continuity of ranks.
    res = client().post('/api/addfavorite', json=fav_coffee,
                        headers={'x-access-token': token})
    assert res.status_code == 201  # Success
    res = client().post('/api/addfavorite', json=fav_guido,
                        headers={'x-access-token': token})
    assert res.status_code == 201  # Success

    # Get favorites No filtering
    res = client().get('/api/favorites', headers={'x-access-token': token})
    res_data = json.loads(res.data.decode('utf-8').replace("'", '\"'))
    assert len(res_data['favorites']) == 4
    assert {favorite['title'] for favorite in res_data['favorites']} == {
                'New York', 'Pizza', 'Coffee', 'Guido Van Rossum'}

    # Get favorites: Filtering (By categories)
    res = client().get('/api/favorites/food/modified_at&desc/&',
                       headers={'x-access-token': token})
    res_data = json.loads(res.data.decode('utf-8').replace("'", '\"'))
    assert len(res_data['favorites']) == 2
    assert {favorite['title'] for favorite in res_data['favorites']} == {
                'Pizza', 'Coffee'}

    # Get favorites: Filtering (By categories and search for coffee)
    res = client().get('/api/favorites/food/modified_at&desc/coffee',
                       headers={'x-access-token': token})
    res_data = json.loads(res.data.decode('utf-8').replace("'", '\"'))
    assert len(res_data['favorites']) == 1
    assert {favorite['title'] for favorite in res_data['favorites']} == {
                'Coffee'}
    # Get favorites: Filtering (By categories and sort by rank (ascending))

    res = client().get('/api/favorites/food/rank&asc/&',
                       headers={'x-access-token': token})
    res_data = json.loads(res.data.decode('utf-8').replace("'", '\"'))
    assert [favorite['title'] for favorite in res_data['favorites']] == [
                'Coffee', 'Pizza']
    assert [favorite['rank'] for favorite in res_data['favorites']] == [1, 2]


def test_remove_favorite(client, user):
    client().post('/api/register', json=user)
    token = login(client, user)

    fav_pizza = {
        'title': 'Pizza',
        'description': 'Lunch time!',
        'category': {'name': 'Food'},
        'rank': 1
    }
    fav_coffee = {
        'title': 'Coffee',
        'description': 'To code!',
        'category': {'name': 'Food'},
        'rank': 1
    }
    # Remove Non existing favorite
    res = client().delete('/api/removefavorite/food/pizza',
                          headers={'x-access-token': token})
    assert res.status_code == 404  # Failure

    # Add Favorites
    client().post('/api/addfavorite', json=fav_pizza,
                  headers={'x-access-token': token})
    client().post('/api/addfavorite', json=fav_coffee,
                  headers={'x-access-token': token})
    # Remove existing favorite
    res = client().delete('/api/removefavorite/food/coffee',
                          headers={'x-access-token': token})
    assert res.status_code == 200

    # Get favorites
    res = client().get('/api/favorites', headers={'x-access-token': token})
    res_data = json.loads(res.data.decode('utf-8').replace("'", '\"'))
    assert len(res_data['favorites']) == 1
    # Pizza's rank should now be 1
    assert [favorite['rank'] for favorite in res_data['favorites']] == [1]


def test_edit_favorite(client, user):
    client().post('/api/register', json=user)
    token = login(client, user)

    fav_pizza = {
        'title': 'Pizza',
        'description': 'Lunch time!',
        'category': {'name': 'Food'},
        'rank': 1
    }
    fav_coffee = {
        'title': 'Coffee',
        'description': 'To code!',
        'category': {'name': 'Food'},
        'rank': 1
    }
    # Edit a non existing favorite
    res = client().put('/api/editfavorite',
                       json={'title': 'nothing', 'category': 'Place',
                             'new_title': 'One good place'},
                       headers={'x-access-token': token})
    assert res.status_code == 404  # Failure

    # Add Favorites
    client().post('/api/addfavorite', json=fav_pizza,
                  headers={'x-access-token': token})
    client().post('/api/addfavorite', json=fav_coffee,
                  headers={'x-access-token': token})

    # Edit favorite to used title
    res = client().put('/api/editfavorite',
                       json={'title': 'Pizza', 'category': 'Food',
                             'new_title': 'Coffee'},
                       headers={'x-access-token': token})
    assert res.status_code == 202  # Failure

    # Edit favorite re-rank
    res = client().put('/api/editfavorite',
                       json={'title': 'Pizza', 'category': 'Food',
                             'new_rank': 1},
                       headers={'x-access-token': token})
    assert res.status_code == 200
    res = client().get('/api/favorites/food/rank&asc/&',
                       headers={'x-access-token': token})
    res_data = json.loads(res.data.decode('utf-8').replace("'", '\"'))
    assert [favorite['title'] for favorite in res_data['favorites']] == [
               'Pizza',  'Coffee']

    # Edit favorite re-rank to greater than number of favorites
    res = client().put('/api/editfavorite',
                       json={'title': 'Pizza', 'category': 'Food',
                             'new_rank': 5},
                       headers={'x-access-token': token})
    res = client().get('/api/favorites/food/rank&asc/&',
                       headers={'x-access-token': token})
    res_data = json.loads(res.data.decode('utf-8').replace("'", '\"'))
    assert [favorite['title'] for favorite in res_data['favorites']] == [
                'Coffee', 'Pizza']
    assert [favorite['rank'] for favorite in res_data['favorites']] == [1, 2]


def test_get_auditlogs(client, user):
    client().post('/api/register', json=user)
    token = login(client, user)
    res = client().get('/api/logs', headers={'x-access-token': token})
    assert res.status_code == 200

    res_data = json.loads(res.data.decode('utf-8').replace("'", '\"'))
    # 4 corresponding to user creation as well as the creation
    # of categories: food, place and person.
    assert len(res_data['auditlogs']) == 4


def test_get_category_favorites_and_logs(client, user):
    client().post('/api/register', json=user)
    token = login(client, user)

    fav_pizza = {
        'title': 'Pizza',
        'description': 'Lunch time!',
        'category': {'name': 'Food'},
        'rank': 1
    }
    fav_coffee = {
        'title': 'Coffee',
        'description': 'To code!',
        'category': {'name': 'Food'},
        'rank': 1
    }
    # Add Favorites
    client().post('/api/addfavorite', json=fav_pizza,
                  headers={'x-access-token': token})
    client().post('/api/addfavorite', json=fav_coffee,
                  headers={'x-access-token': token})

    # Get non existing category
    res = client().get('/api/categories/nothing',
                       headers={'x-access-token': token})
    assert res.status_code == 404

    # Get non existing favorite
    res = client().get('/api/categories/food/burger',
                       headers={'x-access-token': token})
    assert res.status_code == 404

    # Get existing category
    res = client().get('/api/categories/food',
                       headers={'x-access-token': token})
    assert res.status_code == 200
    res_data = json.loads(res.data.decode('utf-8').replace("'", '\"'))
    # Food should have two favorites now: pizza and coffee.
    assert len(res_data['favorites']) == 2
    assert {favorite['title'] for favorite in res_data['favorites']} == {
                'Coffee', 'Pizza'}
    # Food auditlogs should have 4 entries: for
    # food category's creation, pizza's creation, pizza's rank update
    # and coffee's creation
    assert len(res_data['auditlogs']) == 4

    # Get existing favorite
    res = client().get('/api/categories/food/1',
                       headers={'x-access-token': token})
    assert res.status_code == 200
    res_data = json.loads(res.data.decode('utf-8').replace("'", '\"'))
    assert res_data['favorite']['title'] == 'Coffee'
    # Auditlogs has 1 item corresponding to coffee's creation.
    assert len(res_data['auditlogs']) == 1
